# Bug Tower Defense

## How to play

Keep your core alive! Hordes of alien bugs will be coming to destroy it if you are not careful.

Build towers to prevent bugs getting to the core, if 9 enemies get through, you will lose.

Survive 5 waves.

T => Build Laser Turret

Laser turrets are your basic turret and cost 25 credits. They shoot fast, but don't do much damage.

E => Build Energy Turret

Energy turrets are your AOE turret and cost 200 credits. They shoot slow moving projectiles
that damage all enemies around the target when they land.

M => Build Magnet Turret

Magnet turrets slow enemies that pass by them down.

U => Upgrade turrets

Upgrade a turret. Upgrades cost 2x base cost for level 2, 3x base cost for level 3