mod components;
mod input;
mod resources;
mod states;
mod systems;
mod utils;

use std::env;

use amethyst::{
    core::transform::TransformBundle,
    input::InputBundle,
    prelude::*,
    renderer::{
        plugins::{RenderFlat2D, RenderToWindow},
        types::DefaultBackend,
        RenderingBundle,
    },
    ui::{RenderUi, UiBundle},
    utils::application_root_dir,
};

fn main() -> amethyst::Result<()> {
    env::set_var("WINIT_HIDPI_FACTOR", "1");
    amethyst::start_logger(Default::default());

    let app_root = application_root_dir()?;

    let assets_dir = app_root.join("assets");
    let config_dir = app_root.join("config");
    let display_config_path = config_dir.join("display.ron");
    let input_config = config_dir.join("bindings.ron");

    let input_bundle =
        InputBundle::<input::StandardBindingTypes>::new().with_bindings_from_file(input_config)?;

    let game_data = GameDataBuilder::default()
        .with_bundle(input_bundle)?
        .with_bundle(TransformBundle::new())?
        .with_bundle(UiBundle::<input::StandardBindingTypes>::new())?
        .with_bundle(
            RenderingBundle::<DefaultBackend>::new()
                .with_plugin(
                    RenderToWindow::from_config_path(display_config_path)?
                        .with_clear([0.34, 0.36, 0.52, 1.0]),
                )
                .with_plugin(RenderFlat2D::default())
                .with_plugin(RenderUi::default()),
        )?
        .with_system_desc(amethyst::core::HideHierarchySystemDesc, "hide_hierarchy", &[]);

    let mut game: CoreApplication<
        '_,
        amethyst::GameData<'static, 'static>,
        amethyst::StateEvent<input::StandardBindingTypes>,
        amethyst::StateEventReader<input::StandardBindingTypes>,
    > = CoreApplication::new(assets_dir, states::InitialState::default(), game_data)?;
    game.run();

    Ok(())
}
