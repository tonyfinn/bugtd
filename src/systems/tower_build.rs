use amethyst::{
    core::transform::Transform, ecs::prelude::*, input::InputHandler, renderer::SpriteRender,
    core::timing::Time,
};

use crate::{
    components::{Tower, TowerType},
    input::{StandardActions, StandardBindingTypes},
    resources::{AssetManager,Pathfinder, Scoreboard},
    states::{BuildGhostTower, BuildMode},
    utils::GridObject,
};

type BuildSystemData<'a> = (
    WriteExpect<'a, Pathfinder>,
    ReadExpect<'a, AssetManager>,
    ReadExpect<'a, InputHandler<StandardBindingTypes>>,
    ReadExpect<'a, BuildGhostTower>,
    Read<'a, Time>,
    Write<'a, Scoreboard>,
    WriteStorage<'a, SpriteRender>,
    WriteStorage<'a, Tower>,
    WriteStorage<'a, Transform>,
    Entities<'a>,
);

use std::time::Duration;

pub struct TowerBuildSystem {
    last_action_time: Duration
}

impl Default for TowerBuildSystem {
    fn default() -> TowerBuildSystem {
        TowerBuildSystem {
            last_action_time: Duration::from_secs(0),
        }
    }
}

impl TowerBuildSystem {
    fn is_position_valid(&self, towers: &WriteStorage<Tower>, grid_x: f32, grid_y: f32) -> bool {
        if grid_x >= 39.0 || grid_y >= 23.0 {
            return false;
        }
        let new_tower_x = grid_x as i32;
        let new_tower_y = grid_y as i32;
        for tower in towers.join() {
            let other_tower_x = tower.grid_x as i32;
            let other_tower_y = tower.grid_y as i32;
            if (other_tower_x >= new_tower_x - 1)
                && (other_tower_x < new_tower_x + 2)
                && (other_tower_y >= new_tower_y - 1)
                && (other_tower_y < new_tower_y + 2)
            {
                return false;
            }
        }
        true
    }

    fn do_upgrade(
        &mut self,
        (mut pf, assets, input, ghost, time, mut scoreboard, mut sprites, mut towers, mut transforms, entities): BuildSystemData,
    ) {
        if let Some((x, y)) = input.mouse_position() {
            let grid_x = (x / 32.0).floor();
            let grid_y = (y / 32.0).floor();
            let mut new_sprite = assets.laser_tower_invalid_sprite();
            let mut new_transform = Transform::default();
            new_transform.set_translation_xyz((grid_x * 32.0) + 32.0, (grid_y * 32.0) + 32.0, 0.5);
            let mut tower_to_upgrade: Option<Entity> = None;
            for (tower, transform, entity) in (&mut towers, &transforms, &entities).join() {
                if tower.contains_point(grid_x as u16, grid_y as u16) {
                    let cost = tower.ty.cost() * ((tower.level as u32) + 1);
                    let credits = scoreboard.credits;
                    let has_credits = credits >= cost;
                    if has_credits && tower.level < 3 {
                        new_sprite = assets.laser_tower_upgrade_sprite();
                        tower_to_upgrade = Some(entity);
                    }
                    new_transform = transform.clone();
                    new_transform.set_translation_z(2.0);
                    break;
                }
            }
            if let Some((entity, tower)) = tower_to_upgrade.and_then(|t| Some((t, towers.get_mut(t)?))) {
                if input
                    .action_is_down(&StandardActions::PlaceTower)
                    .unwrap_or(false) 
                    && (time.absolute_time() - self.last_action_time) > Duration::from_secs_f32(0.5)
                {
                    let cost = tower.ty.cost() * ((tower.level as u32) + 1);
                    if scoreboard.credits >= cost && tower.level < 3{
                        tower.level += 1;
                        sprites.insert(entity, assets.sprite_for_tower_type(tower.ty, tower.level));
                        scoreboard.credits -= cost;
                        self.last_action_time = time.absolute_time();
                    }
                }
            }
            sprites.insert(ghost.ghost_tower, new_sprite).unwrap();
            transforms.insert(ghost.ghost_tower, new_transform).unwrap();
        }
    }

    fn do_build(
        &mut self,
        tty: TowerType,
        (mut pf, assets, input, ghost, time, mut scoreboard, mut sprites, mut towers, mut transforms, entities): BuildSystemData,
    ) {
        if let Some((x, y)) = input.mouse_position() {
            let cost = tty.cost();
            let credits = scoreboard.credits;
            let has_credits = credits >= cost;
            let grid_x = (x / 32.0).floor();
            let grid_y = (y / 32.0).floor();
            let mut new_transform = Transform::default();
            new_transform.set_translation_xyz((grid_x * 32.0) + 32.0, (grid_y * 32.0) + 32.0, 0.5);
            transforms.insert(ghost.ghost_tower, new_transform).unwrap();
            let position_valid = self.is_position_valid(&towers, grid_x, grid_y);
            let new_sprite = if position_valid && has_credits {
                assets.sprite_for_tower_type(tty, 1)
            } else {
                assets.laser_tower_invalid_sprite()
            };
            sprites.insert(ghost.ghost_tower, new_sprite).unwrap();

            if position_valid
                && has_credits
                && input
                    .action_is_down(&StandardActions::PlaceTower)
                    .unwrap_or(false)
                && (time.absolute_time() - self.last_action_time) > Duration::from_secs_f32(0.5)
            {
                let new_tower = Tower::new(tty, grid_x as u16, grid_y as u16);
                pf.add_obstacle(&new_tower);
                entities
                    .build_entity()
                    .with(new_tower.transform(), &mut transforms)
                    .with(assets.sprite_for_tower_type(tty, 1), &mut sprites)
                    .with(new_tower, &mut towers)
                    .build();
                scoreboard.credits -= cost;
                self.last_action_time = time.absolute_time();
            }
        }
    }
}

impl<'a> System<'a> for TowerBuildSystem {
    type SystemData = BuildSystemData<'a>;

    fn run(
        &mut self,
        (pf, assets, input, ghost, time, scoreboard, sprites, towers, transforms, entities): Self::SystemData,
    ) {
        match ghost.build_mode {
            BuildMode::Tower(tty) => {
                self.do_build(tty, (pf, assets, input, ghost, time, scoreboard, sprites, towers, transforms, entities))
            },
            BuildMode::Upgrade => {
                self.do_upgrade((pf, assets, input, ghost, time, scoreboard, sprites, towers, transforms, entities))
            }
        }
    }
}
