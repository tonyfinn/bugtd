use amethyst::{
    core::{ecs::prelude::*, timing::Time, transform::Transform},
    renderer::SpriteRender,
};

use crate::components::{Enemy, Wave};
use crate::resources::{AssetManager, GameStatus, Scoreboard};
use std::collections::HashMap;
use std::time::Duration;

const SPAWN_COOLDOWN: Duration = Duration::from_secs(2);

pub struct WaveSystem {
    last_spawn_time: Duration,
    spawned_count: f32,
    next_id: u32,
}

impl Default for WaveSystem {
    fn default() -> WaveSystem {
        WaveSystem {
            last_spawn_time: Duration::from_secs(0),
            spawned_count: 0.0,
            next_id: 0,
        }
    }
}

impl WaveSystem {
    fn spawn_enemy(
        &mut self,
        wave: &mut Wave,
        enemies: &mut WriteStorage<Enemy>,
        transforms: &mut WriteStorage<Transform>,
        sprites: &mut WriteStorage<SpriteRender>,
        assets: &AssetManager,
        entities: &Entities,
    ) {
        let ty = wave.enemy_list.pop_front().unwrap();
        let hp = (20.0 * wave.multiplier).floor() as u16;
        let enemy = Enemy::new(self.next_id, hp, ty, wave.id, wave.multiplier);
        self.next_id += 1;
        println!("Enemy {:?} spawned", enemy);

        let mut transform = Transform::default();
        let x_pos = (self.spawned_count * 37.0) % 32.0;
        let y_pos: f32 = (self.spawned_count * 98321.0) % 720.0;
        transform.set_translation_xyz(x_pos + 16.0, y_pos + 16.0, 0.0);

        entities
            .build_entity()
            .with(enemy.ty.sprite(assets), sprites)
            .with(transform, transforms)
            .with(enemy, enemies)
            .build();
    }
}

impl<'a> System<'a> for WaveSystem {
    type SystemData = (
        WriteStorage<'a, Wave>,
        WriteStorage<'a, Enemy>,
        WriteStorage<'a, Transform>,
        WriteStorage<'a, SpriteRender>,
        Write<'a, Scoreboard>,
        Entities<'a>,
        ReadExpect<'a, AssetManager>,
        Read<'a, Time>,
    );

    fn run(
        &mut self,
        (mut waves, mut enemies, mut transforms, mut sprites, mut scoreboard, entities, assets, time): Self::SystemData,
    ) {
        let mut count_by_wave: HashMap<u16, usize> = HashMap::new();
        let mut count_uncleared_waves = 0;
        for enemy in enemies.join() {
            let wave_id: u16 = enemy.wave;
            let existing_count = count_by_wave.get(&wave_id).map_or(0, |x| *x);
            count_by_wave.insert(wave_id, existing_count + 1);
        }
        let current_game_time = time.absolute_time();
        for wave in (&mut waves).join() {
            if wave.started {
                let remaining_enemy_count = count_by_wave.get(&wave.id).map_or(0, |x| *x);
                let to_spawn_count = wave.enemy_list.len();
                if !wave.cleared && (to_spawn_count + remaining_enemy_count == 0) {
                    wave.cleared = true;
                } else if to_spawn_count > 0 {
                    if current_game_time - self.last_spawn_time > SPAWN_COOLDOWN {
                        self.spawn_enemy(
                            wave,
                            &mut enemies,
                            &mut transforms,
                            &mut sprites,
                            &assets,
                            &entities,
                        );
                        self.last_spawn_time = current_game_time;
                        self.spawned_count += 1.0;
                    }
                }
            } else if current_game_time >= wave.start_time {
                wave.started = true;
                if wave.id > scoreboard.wave {
                    scoreboard.wave = wave.id;
                }
                scoreboard.time_to_next_wave = None
            } else {
                let time_to_wave = wave.start_time - current_game_time;
                if scoreboard.time_to_next_wave.is_none() || time_to_wave < scoreboard.time_to_next_wave.unwrap() {
                    scoreboard.time_to_next_wave = Some(time_to_wave);
                }
            }
            if !wave.cleared {
                count_uncleared_waves += 1;
            }
        }
        if count_uncleared_waves == 0 {
            scoreboard.status = GameStatus::GameOverWin;
        }
    }
}
