use amethyst::core::Transform;
use amethyst::ecs::prelude::*;

use crate::components::{Core, Enemy, Path};
use crate::resources::Pathfinder;
use crate::utils::{GridObject, GridPos};

pub struct EnemyPathfindingSystem;

impl<'a> System<'a> for EnemyPathfindingSystem {
    type SystemData = (
        Entities<'a>,
        WriteExpect<'a, Pathfinder>,
        WriteStorage<'a, Path>,
        ReadStorage<'a, Transform>,
        ReadStorage<'a, Core>,
        ReadStorage<'a, Enemy>,
    );

    fn run(
        &mut self,
        (entities, mut pathfinder, mut paths, transforms, cores, enemies): Self::SystemData,
    ) {
        let mut new_paths = Vec::new();
        for (entity, _, enemy, transform) in (&entities, !&paths, &enemies, &transforms).join() {
            let start_pos = GridPos::from_transform(transform);
            let cores: Vec<&Core> = cores.join().collect();
            let target_core = cores[(enemy.id as usize) % cores.len()];
            let path = pathfinder.find_route(start_pos, target_core);
            match path {
                Some(p) => {
                    new_paths.push((entity, p));
                }
                None => eprintln!(
                    "Could not path from {:?} to {:?}",
                    start_pos,
                    target_core.grid_pos()
                ),
            }
        }

        for (entity, p) in new_paths {
            paths.insert(entity, p);
        }
    }
}
