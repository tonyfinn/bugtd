use amethyst::{
    core::{Time, Transform},
    ecs::prelude::*,
    renderer::SpriteRender,
};

use crate::components::Projectile;
use crate::resources::AssetManager;

pub struct ProjectileSystem;

impl<'a> System<'a> for ProjectileSystem {
    type SystemData = (
        Entities<'a>,
        Read<'a, Time>,
        ReadExpect<'a, AssetManager>,
        WriteStorage<'a, Projectile>,
        WriteStorage<'a, Transform>,
        WriteStorage<'a, SpriteRender>
    );

    fn run(&mut self, (entities, time, assets, mut projectiles, mut transforms, mut sprites): Self::SystemData) {
        let delta_seconds = time.delta_seconds();

        for (entity, mut projectile, transform, mut sprite) in
            (&*entities, &mut projectiles, &mut transforms, &mut sprites).join()
        {
            if projectile.elapsed_time > projectile.ty.duration() {
                println!("Projectile expired");
                entities.delete(entity).unwrap();
            } else {
                let amount_to_advance = projectile.ty.speed() * delta_seconds;
                transform.move_right(amount_to_advance);
                projectile.elapsed_time += delta_seconds;
                projectile.frame += 1;
                *sprite = assets.projectile_frame(projectile.ty, projectile.frame);
            }
        }
    }
}
