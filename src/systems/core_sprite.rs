use amethyst::{ecs::prelude::*, renderer::SpriteRender};

use crate::{components::Core, resources::AssetManager};

pub struct CoreSpriteSystem;

impl<'a> System<'a> for CoreSpriteSystem {
    type SystemData = (
        WriteStorage<'a, SpriteRender>,
        ReadStorage<'a, Core>,
        ReadExpect<'a, AssetManager>,
    );

    fn run(&mut self, (mut sprites, cores, assets): Self::SystemData) {
        for (sprite, core) in (&mut sprites, &cores).join() {
            *sprite = assets.core_sprite(core.hp);
        }
    }
}
