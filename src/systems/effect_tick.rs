use amethyst::ecs::prelude::*;
use amethyst::core::timing::Time;

use crate::components::{Effect, EffectType, Enemy, SpriteRender};
use crate::resources::{AssetManager};

use std::time::Duration;

pub struct EffectTickSystem;

impl<'a> System<'a> for EffectTickSystem {
    type SystemData = (
        Entities<'a>,
        Read<'a, Time>,
        ReadExpect<'a, AssetManager>,
        WriteStorage<'a, Effect>,
        WriteStorage<'a, Enemy>,
        WriteStorage<'a, SpriteRender>
    );

    fn run(&mut self, (entities, time, assets, mut effects, mut enemies, mut sprites): Self::SystemData) {
        for enemy in (&mut enemies).join() {
            if let Some(last_time) = enemy.last_slow_time {
                if last_time + Duration::from_secs_f32(EffectType::Slow.duration()) < time.absolute_time() {
                    enemy.slow_amount = 0.0;
                }
            }
        }

        for (entity, effect, sprite) in (&entities, &mut effects, &mut sprites).join() {
            if effect.spawn_time + Duration::from_secs_f32(effect.ty.duration()) < time.absolute_time() {
                entities.delete(entity);
            } else {
                effect.frame += 1;
                *sprite = assets.effect_frame(effect.ty, effect.frame);
            }
        }
    }
}