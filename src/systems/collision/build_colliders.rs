use crate::{
    components::{Collidable, Collider, ColliderType, CollisionHandle, Core, Effect, Enemy, Projectile},
    resources::{CollidableData, CollisionSystem},
};
use amethyst::{
    core::{
        math::{Isometry2, Vector2},
        transform::Transform,
    },
    ecs::prelude::*,
};
use ncollide2d::{
    pipeline::object::CollisionGroups,
    pipeline::GeometricQueryType,
    shape::{Cuboid, ShapeHandle},
};

pub const PIXELS_PER_COLLIDER_UNIT: f32 = 16.0;

fn collider_shape(collision_size: (f32, f32)) -> ShapeHandle<f32> {
    let (wpx, hpx) = collision_size;
    let half_extents = Vector2::new(wpx, hpx) / (PIXELS_PER_COLLIDER_UNIT * 2.0);
    ShapeHandle::new(Cuboid::new(half_extents))
}

fn collider_groups(ty: ColliderType) -> CollisionGroups {
    CollisionGroups::new()
        .with_membership(&[ty.group_id()])
        .with_whitelist(&ty.collides_with())
}

pub fn collider_pos(transform: &Transform) -> Isometry2<f32> {
    let translation = transform.translation().xy() / PIXELS_PER_COLLIDER_UNIT;
    let rotation_angle = transform.rotation().euler_angles().2;
    Isometry2::new(translation, rotation_angle)
}

fn build_collider<T: Collidable>(
    item: &T,
    entity: Entity,
    transform: &Transform,
    world: &mut CollisionSystem,
) -> CollisionHandle {
    let data = CollidableData {
        entity,
        ty: item.collider_type(),
    };
    let (col_handle, _obj) = world.add(
        collider_pos(transform),
        collider_shape(item.collision_size()),
        collider_groups(item.collider_type()),
        GeometricQueryType::Proximity(0.0),
        data,
    );
    col_handle
}

pub struct BuildColliderSystem;

impl<'a> System<'a> for BuildColliderSystem {
    type SystemData = (
        Entities<'a>,
        WriteExpect<'a, CollisionSystem>,
        WriteStorage<'a, Collider>,
        ReadStorage<'a, Transform>,
        ReadStorage<'a, Enemy>,
        ReadStorage<'a, Effect>,
        ReadStorage<'a, Core>,
        ReadStorage<'a, Projectile>,
    );

    fn run(
        &mut self,
        (entities, mut col_world, mut colliders, transforms, enemies, effects, cores, projectiles): Self::SystemData,
    ) {
        let mut colliders_to_add = Vec::new();
        for (entity, enemy, transform, _) in (&entities, &enemies, &transforms, !&colliders).join()
        {
            colliders_to_add.push((
                entity,
                build_collider(enemy, entity, transform, &mut col_world),
            ));
        }

        for (entity, projectile, transform, _) in
            (&entities, &projectiles, &transforms, !&colliders).join()
        {
            colliders_to_add.push((
                entity,
                build_collider(projectile, entity, transform, &mut col_world),
            ));
        }

        for (entity, effect, transform, _) in
            (&entities, &effects, &transforms, !&colliders).join()
        {
            colliders_to_add.push((
                entity,
                build_collider(effect, entity, transform, &mut col_world),
            ));
        }

        for (entity, core, transform, _) in (&entities, &cores, &transforms, !&colliders).join() {
            colliders_to_add.push((
                entity,
                build_collider(core, entity, transform, &mut col_world),
            ));
        }

        for (entity, handle) in colliders_to_add {
            colliders.insert(entity, handle.into()).unwrap();
        }
    }
}
