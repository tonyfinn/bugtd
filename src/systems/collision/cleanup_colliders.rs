use amethyst::ecs::prelude::*;

use crate::{components::CollisionHandle, resources::CollisionSystem};

pub struct CleanupColliderSystem;

impl<'a> System<'a> for CleanupColliderSystem {
    type SystemData = (Entities<'a>, WriteExpect<'a, CollisionSystem>);

    fn run(&mut self, (entities, mut collision_system): Self::SystemData) {
        let mut handles_to_remove = Vec::<CollisionHandle>::new();
        for (handle, obj) in collision_system.collision_objects() {
            let entity = obj.data().clone().entity;
            let entity_id = entity.id();
            if !entities.is_alive(entity) {
                println!("Removing collider for entity {}", entity_id);
                handles_to_remove.push(handle);
            }
        }

        collision_system.remove(&handles_to_remove);
    }
}
