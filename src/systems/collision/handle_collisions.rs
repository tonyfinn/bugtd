use amethyst::ecs::prelude::*;
use amethyst::core::timing::Time;
use amethyst::core::math::Vector3;

use crate::components::{ColliderType, Core, Effect, EffectType, Enemy, Projectile, ProjectileType, Transform, SpriteRender};
use crate::resources::{AssetManager, CollisionSystem, GameStatus, Scoreboard};

use std::time::Duration;

pub struct HandleCollisionSystem;

impl HandleCollisionSystem {
    fn handle_enemy_projectile_collision(
        &self,
        enemies: &mut WriteStorage<Enemy>,
        projectiles: &ReadStorage<Projectile>,
        enemy_entity: Entity,
        projectile_entity: Entity,
        entities: &Entities,
        transforms: &mut WriteStorage<Transform>,
        sprites: &mut WriteStorage<SpriteRender>,
        effects: &mut WriteStorage<Effect>,
        collision_time: Duration,
        assets: &AssetManager,
        scoreboard: &mut Scoreboard,
    ) {
        let enemy = enemies.get_mut(enemy_entity);
        if enemy.is_none() {
            return
        }
        let enemy = enemy.unwrap();
        let projectile = projectiles.get(projectile_entity).unwrap();
        if projectile.ty == ProjectileType::EnergyOrb {
            let effect = Effect::new(EffectType::Damage, projectile.damage(), collision_time);
            let sprite = assets.effect_frame(effect.ty, 0);
            let mut transform = transforms.get(projectile_entity).unwrap().clone();
            transform.set_scale(Vector3::new(2.0, 2.0, 2.0));
            entities.build_entity()
                .with(sprite, sprites)
                .with(effect, effects)
                .with(transform, transforms)
                .build();
        } else {
            enemy.hp = enemy.hp.saturating_sub(projectile.damage());
            if enemy.hp == 0 {
                scoreboard.score += enemy.score_reward();
                scoreboard.credits += enemy.credits_reward(); 
                entities.delete(enemy_entity);
            }
        }
        entities.delete(projectile_entity);
    }
    fn handle_core_enemy_collision(
        &self,
        cores: &mut WriteStorage<Core>,
        enemies: &mut WriteStorage<Enemy>,
        core_entity: Entity,
        enemy_entity: Entity,
        entities: &Entities,
        scoreboard: &mut Scoreboard,
    ) {
        println!("Enemy core collision");
        let core = cores.get_mut(core_entity).unwrap();
        entities.delete(enemy_entity);
        core.hp = core.hp.saturating_sub(1);
        scoreboard.core_power = scoreboard.core_power.saturating_sub(1);
        if core.hp == 0 {
            scoreboard.status = GameStatus::GameOverLoss
        }
    }

    fn handle_effect_enemy_collision(
        &self,
        effects: &WriteStorage<Effect>,
        enemies: &mut WriteStorage<Enemy>,
        effect_entity: Entity,
        enemy_entity: Entity,
        current_time: Duration,
        entities: &Entities,
        scoreboard: &mut Scoreboard,
    ) {
        let effect = effects.get(effect_entity);
        let enemy = enemies.get_mut(enemy_entity);
        match (effect, enemy, effect.map(|e| e.ty)) {
            (Some(effect), Some(enemy), Some(EffectType::Slow)) => {
                enemy.slow_amount = f32::from(effect.strength);
                enemy.last_slow_time = Some(current_time);
            },
            (Some(effect), Some(enemy), Some(EffectType::Damage)) => {
                let last_hit_time = enemy.last_aoe_damage_time.unwrap_or(Duration::from_secs(0));
                if last_hit_time + Duration::from_secs(3) < current_time {
                    enemy.hp -= effect.strength;
                    enemy.last_aoe_damage_time = Some(current_time);
                }
                if enemy.hp == 0 {
                    scoreboard.score += enemy.score_reward();
                    scoreboard.credits += enemy.credits_reward(); 
                    entities.delete(enemy_entity);
                }
            },
            _ => {}
        }
    }
}

impl<'a> System<'a> for HandleCollisionSystem {
    type SystemData = (
        Entities<'a>,
        WriteExpect<'a, CollisionSystem>,
        WriteStorage<'a, Enemy>,
        WriteStorage<'a, Core>,
        WriteStorage<'a, Transform>,
        WriteStorage<'a, SpriteRender>,
        WriteStorage<'a, Effect>,
        Write<'a, Scoreboard>,
        Read<'a, Time>,
        ReadExpect<'a, AssetManager>,
        ReadStorage<'a, Projectile>,
    );

    fn run(
        &mut self,
        (entities, mut col_world, mut enemies, mut cores, mut transforms, mut sprites, mut effects, mut scoreboard, time, assets, projectiles): Self::SystemData,
    ) {
        col_world.update();
        for event in col_world.proximity_events() {
            let data1 = col_world
                .collision_object(event.collider1)
                .unwrap()
                .data()
                .clone();
            let data2 = col_world
                .collision_object(event.collider2)
                .unwrap()
                .data()
                .clone();

            match (data1.ty, data1.entity, data2.ty, data2.entity) {
                (ColliderType::Enemy, eent, ColliderType::Projectile, pent) => {
                    self.handle_enemy_projectile_collision(
                        &mut enemies,
                        &projectiles,
                        eent,
                        pent,
                        &entities,
                        &mut transforms,
                        &mut sprites,
                        &mut effects,
                        time.absolute_time(),
                        &assets,
                        &mut scoreboard,
                    );
                }
                (ColliderType::Projectile, pent, ColliderType::Enemy, eent) => {
                    self.handle_enemy_projectile_collision(
                        &mut enemies,
                        &projectiles,
                        eent,
                        pent,
                        &entities,
                        &mut transforms,
                        &mut sprites,
                        &mut effects,
                        time.absolute_time(),
                        &assets,
                        &mut scoreboard,
                    );
                }
                (ColliderType::Core, core_entity, ColliderType::Enemy, enemy_entity) => self
                    .handle_core_enemy_collision(
                        &mut cores,
                        &mut enemies,
                        core_entity,
                        enemy_entity,
                        &entities,
                        &mut scoreboard,
                    ),
                (ColliderType::Enemy, enemy_entity, ColliderType::Core, core_entity) => self
                    .handle_core_enemy_collision(
                        &mut cores,
                        &mut enemies,
                        core_entity,
                        enemy_entity,
                        &entities,
                        &mut scoreboard,
                    ),
                (ColliderType::Effect, effect_entity, ColliderType::Enemy, enemy_entity) => self
                    .handle_effect_enemy_collision(
                        &mut effects,
                        &mut enemies,
                        effect_entity,
                        enemy_entity,
                        time.absolute_time(),
                        &entities,
                        &mut scoreboard,
                    ),
                (ColliderType::Enemy, enemy_entity, ColliderType::Effect, effect_entity) => self
                    .handle_effect_enemy_collision(
                        &mut effects,
                        &mut enemies,
                        effect_entity,
                        enemy_entity,
                        time.absolute_time(),
                        &entities,
                        &mut scoreboard,
                    ),
                (first, _, second, _) => {
                    eprintln!("Ignoring collision between {:?} and {:?}", first, second)
                }
            }
        }
    }
}
