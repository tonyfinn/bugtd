use amethyst::{core::Transform, ecs::prelude::*};

use crate::{
    components::Collider, resources::CollisionSystem,
    systems::collision::build_colliders::collider_pos,
};

pub struct UpdateColliderSystem;

impl<'a> System<'a> for UpdateColliderSystem {
    type SystemData = (
        ReadStorage<'a, Collider>,
        ReadStorage<'a, Transform>,
        WriteExpect<'a, CollisionSystem>,
    );

    fn run(&mut self, (colliders, transforms, mut collision_detection): Self::SystemData) {
        for (collider, transform) in (&colliders, &transforms).join() {
            collision_detection
                .get_mut(collider.handle)
                .map(|col| col.set_position(collider_pos(transform)));
        }
    }
}
