use amethyst::{
    core::{ecs::prelude::*, math as na, timing::Time, transform::Transform, math::Vector3},
    prelude::*,
    renderer::SpriteRender,
};

use crate::{
    components::{Effect, EffectType, Enemy, Projectile, Tower, TowerType},
    resources::AssetManager,
    utils::{angle_2d, vector2d_between},
};

use std::time::Duration;

struct FireDecision {
    transform: Transform,
    projectile: Projectile,
}


fn nearest_enemy(
    tower_transform: &Transform,
    enemies: &ReadStorage<Enemy>,
    transforms: &WriteStorage<Transform>
) -> Option<(f32, f32)> {
    let mut nearest_enemy: Option<(f32, f32)> = None;
    for (_enemy, enemy_transform) in (enemies, transforms).join() {
        let vector_to_enemy = vector2d_between(tower_transform, enemy_transform);
        let distance = vector_to_enemy.norm();
        let angle = angle_2d(&vector_to_enemy);
        match nearest_enemy {
            Some((min_distance, _rot)) if min_distance < distance => {}
            _ => {
                nearest_enemy = Some((distance, angle));
            }
        }
    }
    nearest_enemy
}
pub struct TowerAttackSystem;

impl TowerAttackSystem {
    fn should_fire_angle(
        &self,
        tower: &Tower,
        tower_transform: &Transform,
        nearest_enemy: (f32, f32)
    ) -> Option<FireDecision> {
        let (distance, angle) = nearest_enemy;
        if distance <= tower.range() {
            let mut transform = Transform::default();
            transform.set_translation(tower_transform.translation().clone());
            transform.set_rotation_2d(angle);
            Some(FireDecision {
                transform,
                projectile: Projectile::new(tower.ty.projectile_type()?, tower.level as u16),
            })
        } else {
            None
        }
    }
}

impl<'a> System<'a> for TowerAttackSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, AssetManager>,
        Read<'a, Time>,
        ReadStorage<'a, Enemy>,
        WriteStorage<'a, Transform>,
        WriteStorage<'a, Tower>,
        WriteStorage<'a, Effect>,
        WriteStorage<'a, Projectile>,
        WriteStorage<'a, SpriteRender>,
    );

    fn run(
        &mut self,
        (entities, assets, time, enemies, mut transforms, mut towers, mut effects, mut projectiles, mut sprites): Self::SystemData,
    ) {
        let current_time = time.absolute_time();
        let mut fire_decisions = vec![];
        let mut effects_to_spawn = Vec::new();
        for (mut tower, tower_transform) in (&mut towers, &transforms).join() {
            let transform = tower_transform.clone();
            let nearest_enemy = nearest_enemy(&tower_transform, &enemies, &transforms);
            if tower.ty.projectile_type().is_some() && nearest_enemy.is_some() {
                let nearest_enemy = nearest_enemy.unwrap();
                if (current_time - tower.last_attack_time > tower.ty.cooldown())
                    || tower.last_attack_time == Duration::from_secs(0)
                {
                    let decision =
                        self.should_fire_angle(&mut tower, &tower_transform, nearest_enemy);

                    if let Some(decision) = decision {
                        fire_decisions.push(decision);
                        tower.last_attack_time = current_time;
                    }
                }
            } else if tower.ty == TowerType::Magnet && nearest_enemy.is_some() {
                let (distance, _) = nearest_enemy.unwrap();
                if distance <= tower.range() {
                    let mut transform = tower_transform.clone();
                    transform.set_scale(Vector3::new(1.5, 1.5, 1.5));
                    effects_to_spawn.push((
                        Effect::new(EffectType::Slow, 25 * (tower.level as u16), current_time),
                        assets.magnet_frame(0),
                        transform,
                    ));
                }
            }
        }
        for (effect, sprite, transform) in effects_to_spawn {
            entities.build_entity()
                .with(effect, &mut effects)
                .with(sprite, &mut sprites)
                .with(transform, &mut transforms)
                .build();
        }
        for decision in fire_decisions {
            entities
                .build_entity()
                .with(
                    assets.projectile_frame(decision.projectile.ty, 0),
                    &mut sprites,
                )
                .with(decision.projectile, &mut projectiles)
                .with(decision.transform, &mut transforms)
                .build();
        }
    }
}
