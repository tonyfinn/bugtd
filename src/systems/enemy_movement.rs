use crate::components::{Enemy, Path, Transform};
use crate::resources::Pathfinder;
use crate::utils::{angle_2d, vector2d_between, GridPos, BASIC_TILE_SIZE};
use std::collections::HashSet;

use amethyst::core::Time;
use amethyst::ecs::prelude::*;

pub struct EnemyMovementSystem;

fn pick_point(enemy: &Enemy, pos: GridPos) -> (f32, f32) {
    let x_offset = ((enemy.id as f32) + (f32::from(pos.y))) % BASIC_TILE_SIZE;
    let y_offset = ((enemy.id as f32) + (f32::from(pos.x))) % BASIC_TILE_SIZE;
    let (tl_x, tl_y) = pos.as_world_px();
    (tl_x + x_offset, tl_y + y_offset)
}

impl<'a> System<'a> for EnemyMovementSystem {
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, Enemy>,
        WriteStorage<'a, Path>,
        WriteStorage<'a, Transform>,
        Read<'a, Time>,
        ReadExpect<'a, Pathfinder>,
    );

    fn run(
        &mut self,
        (entities, mut enemies, mut paths, mut transforms, time, pf): Self::SystemData,
    ) {
        let mut clear_path_entities = HashSet::new();
        for (entity, enemy, path, transform) in
            (&entities, &mut enemies, &mut paths, &mut transforms).join()
        {
            let actual_grid_pos = GridPos::from_transform(transform);
            if !pf.is_path_valid(path) {
                println!("Path expired");
                clear_path_entities.insert(entity);
                continue;
            }
            let next_path = path.components[0];
            let next_target_point = if next_path.pos == actual_grid_pos {
                path.components.pop_front();

                path.components.front().map(|pc| {
                    let point = pick_point(enemy, pc.pos);
                    enemy.target_point = point;
                    point
                })
            } else {
                Some(enemy.target_point)
            };

            if let Some((x, y)) = next_target_point {
                let mut target_transform = Transform::default();
                target_transform.set_translation_x(x);
                target_transform.set_translation_y(y);
                let facing = angle_2d(&vector2d_between(&target_transform, transform));
                transform.set_rotation_2d(facing);
                transform.move_left(enemy.speed() * time.delta_seconds());
            } else {
                clear_path_entities.insert(entity);
            }
        }
        for entity in clear_path_entities {
            paths.remove(entity);
        }
    }
}
