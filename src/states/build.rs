use crate::components::{Tower, TowerType};
use crate::{input, resources::AssetManager};

use amethyst::{
    core::transform::Transform,
    ecs::prelude::*,
    input::{InputEvent, VirtualKeyCode},
    prelude::*,
    renderer::{palette::Srgba, resources::Tint, Transparent},
};

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum BuildMode {
    Tower(TowerType),
    Upgrade,
}

pub struct BuildGhostTower {
    pub ghost_tower: Entity,
    pub build_mode: BuildMode,
}

pub struct BuildState {
    grid: Option<Entity>,
    dispatcher: Option<Dispatcher<'static, 'static>>,
    build_mode: BuildMode,
}

impl BuildState {
    pub fn new(build_mode: BuildMode) -> BuildState {
        BuildState {
            grid: None,
            dispatcher: None,
            build_mode,
        }
    }
}

impl State<GameData<'static, 'static>, StateEvent<crate::input::StandardBindingTypes>>
    for BuildState
{
    fn on_start(&mut self, mut data: StateData<'_, GameData<'_, '_>>) {
        let grid_sprite = data.world.read_resource::<AssetManager>().grid_sprite();
        let mut grid_transform = Transform::default();
        grid_transform.set_translation_xyz(640.0, 640.0, 0.9);
        let grid_entity = data
            .world
            .create_entity()
            .with(grid_sprite)
            .with(grid_transform)
            .build();

        let tower_sprite = data
            .world
            .read_resource::<AssetManager>()
            .sprite_for_build_mode(self.build_mode);
        let mut tower_transform = Transform::default();
        tower_transform.set_translation_xyz(32.0, 32.0, 0.5);
        let ghost_tower = data
            .world
            .create_entity()
            .with(tower_sprite)
            .with(Tint(Srgba::new(0.8, 0.8, 0.8, 0.6)))
            .with(Transparent)
            .with(tower_transform)
            .build();

        data.world.insert(BuildGhostTower {
            ghost_tower,
            build_mode: self.build_mode,
        });

        self.grid = Some(grid_entity);

        let mut dispatcher_builder = DispatcherBuilder::new();
        dispatcher_builder.add(crate::systems::TowerBuildSystem::default(), "tower_build", &[]);
        let mut dispatcher = dispatcher_builder.build();
        dispatcher.setup(&mut data.world);

        self.dispatcher = Some(dispatcher);
    }

    /// Executed on every frame immediately, as fast as the engine will allow (taking into account the frame rate limit).
    fn update(
        &mut self,
        data: StateData<'_, GameData<'_, '_>>,
    ) -> Trans<GameData<'static, 'static>, input::StateEvent> {
        data.data.update(data.world);
        if let Some(d) = &mut self.dispatcher {
            d.run_now(&data.world);
        }
        Trans::None
    }

    fn on_stop(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        self.grid.map(|grid| data.world.entities_mut().delete(grid));
        data.world
            .entities_mut()
            .delete(data.world.read_resource::<BuildGhostTower>().ghost_tower)
            .unwrap();
        self.grid = None;
    }

    fn handle_event(
        &mut self,
        _data: StateData<GameData<'static, 'static>>,
        event: input::StateEvent,
    ) -> Trans<GameData<'static, 'static>, input::StateEvent> {
        match event {
            StateEvent::Input(InputEvent::KeyPressed {
                key_code: VirtualKeyCode::Escape,
                ..
            }) => {
                println!("Exiting tower construction mode");
                Trans::Pop
            }
            _ => Trans::None,
        }
    }
}
