use crate::{
    components::{Core, EnemyType, Map, Tower, TowerType, Wave},
    input,
    resources::{AssetManager, CollisionSystem, Pathfinder, Scoreboard, GameStatus},
    states,
    systems,
    utils::GridObject,
};

use amethyst::{
    core::HiddenPropagate,
    core::transform::Transform,
    ecs::prelude::*,
    input::{InputEvent,InputHandler},
    prelude::*,
    renderer::camera::{Camera, Projection},
    ui::{UiCreator,UiFinder,UiText},
    window::ScreenDimensions,
};

use std::time::Duration;


fn basic_wave_sequence() -> Vec<Wave> {
    vec![
        Wave::new(1, 1.00, Duration::from_secs(25), Duration::from_secs(2), &[
            EnemyType::Normal, EnemyType::Normal, EnemyType::Fast,
            EnemyType::Tank,
            EnemyType::Normal, EnemyType::Normal, EnemyType::Fast,
            EnemyType::Normal, EnemyType::Normal, EnemyType::Fast,
            EnemyType::Tank,
            EnemyType::Normal, EnemyType::Normal, EnemyType::Fast,
        ]),
        Wave::new(
            2,
            1.5,
            Duration::from_secs(55),
            Duration::from_secs(1),
            &[
                EnemyType::Normal, EnemyType::Normal, EnemyType::Fast,
                EnemyType::Tank,
                EnemyType::Normal, EnemyType::Normal, EnemyType::Fast,
                EnemyType::Normal, EnemyType::Normal, EnemyType::Fast,
                EnemyType::Normal, EnemyType::Normal, EnemyType::Fast,
                EnemyType::Tank,
                EnemyType::Normal, EnemyType::Normal, EnemyType::Fast,
            ],
        ),
        Wave::new(
            3,
            1.8,
            Duration::from_secs(80),
            Duration::from_secs_f32(0.5),
            &[
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Tank,
                EnemyType::Normal, EnemyType::Normal, EnemyType::Fast,
                EnemyType::Tank,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Tank,
                EnemyType::Normal, EnemyType::Normal, EnemyType::Fast,
                EnemyType::Tank,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Tank,
                EnemyType::Normal, EnemyType::Normal, EnemyType::Fast,
            ],
        ),
        Wave::new(
            4,
            2.5,
            Duration::from_secs(100),
            Duration::from_secs_f32(0.5),
            &[
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Tank,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Tank,
                EnemyType::Tank,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Tank,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
            ],
        ),
        Wave::new(
            5,
            2.5,
            Duration::from_secs(115),
            Duration::from_secs_f32(0.3),
            &[
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Tank,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Tank,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Tank,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Tank,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Tank,
                EnemyType::Normal, EnemyType::Fast, EnemyType::Fast,
                EnemyType::Tank,
            ],
        ),
    ]
}

pub fn build_camera(world: &mut World) {
    let (width, height) = {
        let dim = world.read_resource::<ScreenDimensions>();
        (dim.width(), dim.height())
    };

    let mut transform = Transform::default();
    transform.set_translation_xyz(0.0, height, 10.0);

    let mut camera = Camera::standard_2d(width, height);
    camera.set_projection(Projection::orthographic(0.0, width, height, 0.0, 0.0, 20.0));

    world.create_entity().with(camera).with(transform).build();
}

pub fn setup_waves(world: &mut World) {
    for wave in basic_wave_sequence() {
        world.create_entity().with(wave).build();
    }
}

pub fn setup_core(world: &mut World) {
    let mut core = Core::new(20, 10);
    core.hp = 9;
    let sprite = world.read_resource::<AssetManager>().core_sprite(9);
    world
        .create_entity()
        .with(core.transform())
        .with(sprite)
        .with(core)
        .build();
}

pub fn setup_map(world: &mut World) {
    let width = 40;
    let height = 24;

    let pathfinder = Pathfinder::new(width, height);
    world.insert(pathfinder);

    let map = Map::new(width, height);
    world.register::<Map>();
    world.create_entity().with(map).build();
}

pub struct GameState {
    score_text: Option<Entity>,
    credit_text: Option<Entity>,
    wave_text: Option<Entity>,
    next_wave_text: Option<Entity>,
    dispatcher: Option<Dispatcher<'static, 'static>>,
    ended: bool,
}

impl Default for GameState {
    fn default() -> GameState {
        GameState {
            score_text: None,
            credit_text: None,
            wave_text: None,
            next_wave_text: None,
            dispatcher: None,
            ended: false
        }
    }
}

impl State<GameData<'static, 'static>, input::StateEvent> for GameState {
    fn on_start(&mut self, mut data: StateData<GameData<'_, '_>>) {
        data.world.exec(|mut creator: UiCreator<'_>| {
            creator.create("ui/gameplay.ron", ());
        });

        let mut dispatcher_builder = DispatcherBuilder::new();
        dispatcher_builder.add(systems::WaveSystem::default(), "waves", &[]);
        dispatcher_builder.add(systems::ProjectileSystem, "projectiles", &[]);
        dispatcher_builder.add(systems::EnemyPathfindingSystem, "ai_pathfinding", &[]);
        dispatcher_builder.add(systems::EffectTickSystem, "effect_ticks", &[]);
        dispatcher_builder.add_barrier();
        dispatcher_builder.add(systems::EnemyMovementSystem, "enemy_movement", &[]);
        dispatcher_builder.add(
            systems::TowerAttackSystem,
            "turret_attack",
            &["enemy_movement"],
        );
        dispatcher_builder.add_barrier();
        dispatcher_builder.add(systems::CleanupColliderSystem, "cleanup_colliders", &[]);
        dispatcher_builder.add(
            systems::UpdateColliderSystem,
            "update_colliders",
            &["cleanup_colliders"],
        );
        dispatcher_builder.add(
            systems::BuildColliderSystem,
            "build_colliders",
            &["cleanup_colliders", "update_colliders"],
        );
        dispatcher_builder.add_barrier();
        dispatcher_builder.add(systems::HandleCollisionSystem, "handle_collisions", &[]);
        dispatcher_builder.add(
            systems::CoreSpriteSystem,
            "core_sprite",
            &["handle_collisions"],
        );
        let mut dispatcher = dispatcher_builder.build();
        dispatcher.setup(&mut data.world);

        self.dispatcher = Some(dispatcher);
        
        let asset_manager = AssetManager::new(&data.world);
        data.world.insert(asset_manager);
        data.world.insert(CollisionSystem::new(0.02));
        build_camera(data.world);
        setup_map(data.world);
        setup_waves(data.world);
        setup_core(data.world);
    }

    fn shadow_update(&mut self, data: StateData<'_, GameData<'_, '_>>) {

        let scoreboard = data.world.read_resource::<Scoreboard>();
        let credits = scoreboard.credits;
        let score = scoreboard.score;
        let wave = scoreboard.wave;
        let tt_next_wave = scoreboard.time_to_next_wave;
        drop(scoreboard);
        
        if self.score_text.is_none() {
            data.world.exec(|finder: UiFinder<'_>| {
                if let Some(entity) = finder.find("score_text") {
                    self.score_text = Some(entity);
                }
            });
        }
        
        if self.credit_text.is_none() {
            data.world.exec(|finder: UiFinder<'_>| {
                if let Some(entity) = finder.find("credit_text") {
                    self.credit_text = Some(entity);
                }
            });
        }
        
        if self.wave_text.is_none() {
            data.world.exec(|finder: UiFinder<'_>| {
                if let Some(entity) = finder.find("wave_text") {
                    self.wave_text = Some(entity);
                }
            });
        }
        
        if self.next_wave_text.is_none() {
            data.world.exec(|finder: UiFinder<'_>| {
                if let Some(entity) = finder.find("next_wave_text") {
                    self.next_wave_text = Some(entity);
                }
            });
        }

        let mut ui_text = data.world.write_storage::<UiText>();
        if let Some(score_text) = self.score_text.and_then(|entity| ui_text.get_mut(entity)) {
            score_text.text = format!("Score: {:>9}", score);
        }
        if let Some(credit_text) = self.credit_text.and_then(|entity| ui_text.get_mut(entity)) {
            credit_text.text = format!("Credits: {:>7}", credits);
        }
        if let Some(wave_text) = self.wave_text.and_then(|entity| ui_text.get_mut(entity)) {
            wave_text.text = format!("Wave: {:>10}", wave);
        }
        if let Some(next_wave_text) = self.next_wave_text.and_then(|entity| ui_text.get_mut(entity)) {
            let seconds_text = match tt_next_wave {
                Some(dur) => format!("00:{:02}", dur.as_secs()),
                None => format!("--:--")
            };
            next_wave_text.text = format!("Next Wave: {:>3}", seconds_text);
        }
        drop(ui_text);
    }

    /// Executed on every frame immediately, as fast as the engine will allow (taking into account the frame rate limit).
    fn update(
        &mut self,
        data: StateData<'_, GameData<'_, '_>>,
    ) -> Trans<GameData<'static, 'static>, input::StateEvent> {
        data.data.update(data.world);
        if let Some(d) = &mut self.dispatcher {
            if !self.ended {
                d.run_now(&data.world);
            }
        }
        let scoreboard = data.world.read_resource::<Scoreboard>();
        let game_over_state = scoreboard.status;
        drop(scoreboard);
        
        if game_over_state != GameStatus::Active {
            data.world.exec(|(finder, mut hides, mut texts): (UiFinder<'_>, WriteStorage<'_, HiddenPropagate>, WriteStorage<'_, UiText>)| {
                if let Some(entity) = finder.find("game_over_screen") {
                    hides.remove(entity);
                }
    
                if let Some(el) = finder.find("game_over_text").and_then(|entity| texts.get_mut(entity)) {
                    let win = game_over_state == GameStatus::GameOverWin;
                    if win {
                        el.text = "You win!".into();
                    }
                }
            });
            self.ended = true;
        }
        Trans::None
    }

    fn handle_event(
        &mut self,
        data: StateData<GameData<'static, 'static>>,
        event: input::StateEvent,
    ) -> Trans<GameData<'static, 'static>, input::StateEvent> {
        match event {
            StateEvent::Input(InputEvent::ActionPressed(input::StandardActions::LaserTower)) => {
                println!("Going to tower construction mode");
                Trans::Push(Box::new(states::BuildState::new(states::BuildMode::Tower(
                    TowerType::Laser,
                ))))
            }
            StateEvent::Input(InputEvent::ActionPressed(input::StandardActions::EnergyTower)) => {
                println!("Going to tower construction mode");
                Trans::Push(Box::new(states::BuildState::new(states::BuildMode::Tower(
                    TowerType::Energy,
                ))))
            }
            StateEvent::Input(InputEvent::ActionPressed(input::StandardActions::MagnetTower)) => {
                println!("Going to tower construction mode");
                Trans::Push(Box::new(states::BuildState::new(states::BuildMode::Tower(
                    TowerType::Magnet,
                ))))
            },
            StateEvent::Input(InputEvent::ActionPressed(input::StandardActions::Upgrade)) => {
                println!("Going to tower construction mode");
                Trans::Push(Box::new(states::BuildState::new(states::BuildMode::Upgrade)))
            },
            StateEvent::Input(InputEvent::MouseButtonReleased(_)) => {
                let input_handler = data.world.read_resource::<InputHandler<input::StandardBindingTypes>>();
                if let Some((x, y)) = input_handler.mouse_position() {
                    if y > 690.0 && y < 754.0 {
                        if x > 505.0 && x < 570.0 {
                            Trans::Push(Box::new(states::BuildState::new(states::BuildMode::Tower(
                                TowerType::Laser,
                            ))))
                        } else if x > 570.0 && x < 635.0 {
                            Trans::Push(Box::new(states::BuildState::new(states::BuildMode::Tower(
                                TowerType::Energy,
                            ))))
                        } else if x > 635.0 && x < 705.0 {
                            Trans::Push(Box::new(states::BuildState::new(states::BuildMode::Tower(
                                TowerType::Magnet,
                            ))))
                        } else {
                            Trans::None
                        }
                    } else {
                        Trans::None
                    }
                } else {
                    Trans::None
                }
            }
            _ => Trans::None,
        }
    }
}
