use amethyst::prelude::*;
use amethyst::ecs::prelude::*;

use crate::states::GameState;

pub struct InitialState {
    game_launched: bool,
}

impl Default for InitialState {
    fn default() -> Self {
        InitialState {
            game_launched: false,
        }
    }
}

impl State<GameData<'static, 'static>, StateEvent<crate::input::StandardBindingTypes>>
    for InitialState
{
    fn update(
        &mut self,
        data: StateData<'_, GameData<'_, '_>>,
    ) -> Trans<GameData<'static, 'static>, crate::input::StateEvent> {
        data.data.update(data.world);
        if !self.game_launched {
            self.game_launched = true;
            Trans::Push(Box::new(GameState::default()))
        } else {
            Trans::None
        }
    }
}