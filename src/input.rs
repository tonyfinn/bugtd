use amethyst::input::BindingTypes;
use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum StandardActions {
    PlaceTower,
    LaserTower,
    EnergyTower,
    MagnetTower,
    Upgrade,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum StandardAxis {}

impl std::fmt::Display for StandardAxis {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::fmt::Display for StandardActions {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug)]
pub struct StandardBindingTypes;

impl Default for StandardBindingTypes {
    fn default() -> StandardBindingTypes {
        StandardBindingTypes {}
    }
}

impl BindingTypes for StandardBindingTypes {
    type Axis = StandardAxis;
    type Action = StandardActions;
}

pub type StateEvent = amethyst::prelude::StateEvent<StandardBindingTypes>;
