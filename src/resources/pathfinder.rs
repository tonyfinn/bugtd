use crate::utils::{GridObject, GridPos, GridSize};

use pathfinding::prelude::astar;
use std::collections::{HashMap, VecDeque};
use std::fmt;
use std::iter::FromIterator;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct PathComponent {
    pub dir: Direction,
    pub pos: GridPos,
}

impl PathComponent {
    fn new(dir: Direction, pos: GridPos) -> PathComponent {
        PathComponent { dir, pos }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Path {
    pub id: u32,
    pub components: VecDeque<PathComponent>,
    pub generation: u16,
}

impl fmt::Display for Path {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Path {{ gen={}, nodes=", self.generation)?;
        for component in self.components.iter() {
            write!(f, "[{}, {}] ", component.pos.x, component.pos.y)?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct PFEntity {
    pos: GridPos,
    size: GridSize,
}

impl PFEntity {
    pub fn contains(&self, pos: &GridPos) -> bool {
        let entity = self;
        (entity.pos.x <= pos.x && entity.pos.x + entity.size.w > pos.x)
            && (entity.pos.y <= pos.y && entity.pos.y + entity.size.h > pos.y)
    }
}

impl<T> From<&T> for PFEntity
where
    T: GridObject,
{
    fn from(gobj: &T) -> PFEntity {
        PFEntity {
            pos: gobj.grid_pos(),
            size: gobj.grid_size(),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct PathEndpoints {
    start: GridPos,
    end: PFEntity,
}

pub struct Pathfinder {
    pub obstacles: Vec<PFEntity>,
    pub cached_paths: HashMap<PathEndpoints, Path>,
    pub pf_costs: HashMap<GridPos, u32>,
    pub generation: u16,
    pub map_width: u16,
    pub map_height: u16,
    pub next_id: u32,
}

impl Pathfinder {
    pub fn new(map_width: u16, map_height: u16) -> Pathfinder {
        Pathfinder {
            obstacles: Vec::new(),
            cached_paths: HashMap::new(),
            pf_costs: HashMap::new(),
            generation: 0,
            next_id: 0,
            map_width,
            map_height,
        }
    }

    pub fn costed_move(&self, dir: Direction, x: u16, y: u16) -> (PathComponent, u32) {
        let pos = GridPos::new(x, y);
        let pf_cost = self.pf_costs.get(&pos).unwrap_or(&0) + 1;
        (PathComponent::new(dir, pos), pf_cost)
    }

    pub fn successors(&self, pos: &GridPos) -> Vec<(PathComponent, u32)> {
        let mut successors = Vec::with_capacity(4);
        if pos.x > 0 {
            successors.push(self.costed_move(Direction::West, pos.x - 1, pos.y));
        }
        if pos.y > 0 {
            successors.push(self.costed_move(Direction::North, pos.x, pos.y - 1));
        }
        if pos.x < self.map_width - 1 {
            successors.push(self.costed_move(Direction::East, pos.x + 1, pos.y));
        }
        if pos.y < self.map_height - 1 {
            successors.push(self.costed_move(Direction::South, pos.x, pos.y + 1));
        }
        successors
    }

    pub fn generate_path(&mut self, endpoints: PathEndpoints) -> Option<Path> {
        let path_components = astar(
            &PathComponent::new(Direction::North, endpoints.start),
            |p| self.successors(&p.pos),
            |p| 0,
            |p| endpoints.end.contains(&p.pos),
        )?;
        Some(Path {
            id: self.next_id,
            components: VecDeque::from_iter(path_components.0.iter().cloned()),
            generation: self.generation,
        })
    }

    pub fn find_route<T: GridObject>(&mut self, start: GridPos, target: &T) -> Option<Path> {
        let end = PFEntity::from(target);
        let path_endpoints = PathEndpoints { start, end };
        let path = match self.cached_paths.get(&path_endpoints) {
            Some(existing_path) => {
                let mut new_path = existing_path.clone();
                new_path.id = self.next_id;
                Some(new_path)
            }
            None => self.generate_path(path_endpoints).map(|p| {
                println!("found new path: {}", p);
                p
            }),
        };
        path
    }

    pub fn is_path_valid(&self, p: &Path) -> bool {
        p.generation == self.generation
    }

    pub fn add_obstacle<T: GridObject>(&mut self, obstacle: &T) {
        let entity = PFEntity::from(obstacle);

        for x in entity.pos.x..(entity.pos.x + entity.size.w) {
            for y in entity.pos.y..(entity.pos.y + entity.size.h) {
                println!("Obstacle added to {} {}", x, y);
                self.pf_costs
                    .insert(GridPos::new(x, y), obstacle.pathfinding_cost());
                println!("{:?}", self.pf_costs);
            }
        }
        self.cached_paths.clear();
        self.generation += 1
    }
}
