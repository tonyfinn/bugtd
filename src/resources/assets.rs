use amethyst::{
    assets::{AssetStorage, Handle, Loader},
    prelude::*,
    renderer::{ImageFormat, SpriteRender, SpriteSheet, SpriteSheetFormat, Texture},
};

use crate::components::{ProjectileType, EffectType, TowerType};
use crate::states::BuildMode;

pub fn load_texture<N>(name: N, world: &World) -> Handle<Texture>
where
    N: Into<String>,
{
    let loader = world.read_resource::<Loader>();
    loader.load(
        name,
        ImageFormat::default(),
        (),
        &world.read_resource::<AssetStorage<Texture>>(),
    )
}

pub fn load_sprite_sheet<N>(
    name: N,
    world: &World,
    ss_texture: Handle<Texture>,
) -> Handle<SpriteSheet>
where
    N: Into<String>,
{
    let loader = world.read_resource::<Loader>();
    loader.load(
        name,
        SpriteSheetFormat(ss_texture),
        (),
        &world.read_resource(),
    )
}
pub struct AssetManager {
    attack_ss: Handle<SpriteSheet>,
    tower_ss: Handle<SpriteSheet>,
    enemy_ss: Handle<SpriteSheet>,
    grid_ss: Handle<SpriteSheet>,
}

impl AssetManager {
    pub fn new(world: &World) -> AssetManager {
        AssetManager {
            attack_ss: load_sprites("attacks", world),
            enemy_ss: load_sprites("enemies", world),
            grid_ss: load_sprites("grid", world),
            tower_ss: load_sprites("towers", world),
        }
    }

    pub fn laser_tower_sprite(&self, level: usize) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.tower_ss.clone(),
            sprite_number: 0 + (level - 1),
        }
    }

    pub fn energy_tower_sprite(&self, level: usize) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.tower_ss.clone(),
            sprite_number: 3 + (level - 1),
        }
    }

    pub fn magnet_tower_sprite(&self, level: usize) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.tower_ss.clone(),
            sprite_number: 6 + (level - 1),
        }
    }

    pub fn laser_tower_invalid_sprite(&self) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.tower_ss.clone(),
            sprite_number: 9,
        }
    }

    pub fn laser_tower_upgrade_sprite(&self) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.tower_ss.clone(),
            sprite_number: 10,
        }
    }

    pub fn sprite_for_tower_type(&self, tty: TowerType, level: usize) -> SpriteRender {
        match tty {
            TowerType::Laser => self.laser_tower_sprite(level),
            TowerType::Energy => self.energy_tower_sprite(level),
            TowerType::Magnet => self.magnet_tower_sprite(level),
        }
    }

    pub fn sprite_for_build_mode(&self, bm: BuildMode) -> SpriteRender {
        match bm {
            BuildMode::Tower(tty) => self.sprite_for_tower_type(tty, 1),
            BuildMode::Upgrade => self.laser_tower_upgrade_sprite(),
        }
    }

    pub fn core_sprite(&self, hp: usize) -> SpriteRender {
        let core_sprite_base_offset = 11;
        let specific_core_sprite_offset = 9 - hp;
        SpriteRender {
            sprite_sheet: self.tower_ss.clone(),
            sprite_number: core_sprite_base_offset + specific_core_sprite_offset,
        }
    }

    pub fn enemy_sprite(&self) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.enemy_ss.clone(),
            sprite_number: 0,
        }
    }

    pub fn fast_enemy_sprite(&self) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.enemy_ss.clone(),
            sprite_number: 1,
        }
    }

    pub fn tank_enemy_sprite(&self) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.enemy_ss.clone(),
            sprite_number: 2,
        }
    }

    pub fn grid_sprite(&self) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.grid_ss.clone(),
            sprite_number: 0,
        }
    }

    pub fn laser_frame(&self, frame: usize) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.attack_ss.clone(),
            sprite_number: frame % 5,
        }
    }

    pub fn energy_frame(&self, frame: usize) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.attack_ss.clone(),
            sprite_number: 5 + (frame % 4),
        }
    }

    pub fn projectile_frame(&self, pty: ProjectileType, frame: usize) -> SpriteRender {
        match pty {
            ProjectileType::Laser => self.laser_frame(frame / 5),
            ProjectileType::EnergyOrb => self.energy_frame(frame / 5),
        }
    }

    pub fn magnet_frame(&self, frame: usize) -> SpriteRender {
        SpriteRender {
            sprite_sheet: self.attack_ss.clone(),
            sprite_number: 9 + (frame % 4),
        }
    }

    pub fn effect_frame(&self, ety: EffectType, frame: usize) -> SpriteRender {
        match ety {
            EffectType::Damage => self.energy_frame(frame / 5),
            EffectType::Slow => self.magnet_frame(frame / 20)
        }
    }
}

fn load_sprites(name: &str, world: &World) -> Handle<SpriteSheet> {
    let texture = load_texture(&format!("textures/{}.png", name), world);
    load_sprite_sheet(&format!("spritesheets/{}.ron", name), world, texture)
}
