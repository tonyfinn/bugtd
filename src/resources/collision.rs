use amethyst::{
    assets::{AssetStorage, Handle, Loader},
    ecs::prelude::Entity,
    prelude::*,
    renderer::{ImageFormat, SpriteRender, SpriteSheet, SpriteSheetFormat, Texture},
};

use crate::components::ColliderType;

use ncollide2d::world::CollisionWorld;

pub struct CollidableData {
    pub entity: Entity,
    pub ty: ColliderType,
}

pub type CollisionSystem = CollisionWorld<f32, CollidableData>;
