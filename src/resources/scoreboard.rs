use std::time::Duration;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum GameStatus {
    GameOverLoss,
    GameOverWin,
    Active,
}

pub struct Scoreboard {
    pub score: u32,
    pub wave: u16,
    pub time_to_next_wave: Option<Duration>,
    pub core_power: u16,
    pub credits: u32,
    pub status: GameStatus,
}

impl Default for Scoreboard {
    fn default() -> Scoreboard {
        Scoreboard {
            score: 0,
            wave: 1,
            credits: 500,
            core_power: 9,
            time_to_next_wave: None,
            status: GameStatus::Active,
        }
    }
}