use amethyst::{core::ecs::prelude::*, renderer::SpriteRender};

use crate::{
    components::{Collidable, ColliderType},
    resources::AssetManager,
};

use std::time::Duration;

#[derive(Debug, Clone, Copy)]
pub enum EnemyType {
    Normal,
    Fast,
    Tank,
}

impl EnemyType {
    pub fn base_hp(&self) -> u16 {
        match self {
            EnemyType::Normal => 75,
            EnemyType::Fast => 50,
            EnemyType::Tank => 200,
        }
    }
    pub fn sprite(&self, assets: &AssetManager) -> SpriteRender {
        match self {
            EnemyType::Fast => assets.fast_enemy_sprite(),
            EnemyType::Normal => assets.enemy_sprite(),
            EnemyType::Tank => assets.tank_enemy_sprite(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Enemy {
    pub id: u32,
    pub hp: u16,
    pub ty: EnemyType,
    pub wave: u16,
    pub wave_multiplier: f32,
    pub target_point: (f32, f32),
    pub slow_amount: f32,
    pub last_slow_time: Option<Duration>,
    pub last_aoe_damage_time: Option<Duration>,
}

impl Enemy {
    pub fn new(id: u32, hp: u16, ty: EnemyType, wave: u16, wave_multiplier: f32) -> Enemy {
        Enemy {
            id,
            hp,
            ty,
            wave,
            wave_multiplier,
            target_point: (0.0, 0.0),
            slow_amount: 0.0,
            last_slow_time: None,
            last_aoe_damage_time: None,
        }
    }

    pub fn speed(&self) -> f32 {
        let base_speed = match self.ty {
            EnemyType::Fast => 50.0,
            EnemyType::Normal => 30.0,
            EnemyType::Tank => 15.0,
        };

        let slow_amount = (100.0 - self.slow_amount) / 100.0;

        base_speed * slow_amount
    }

    pub fn credits_reward(&self) -> u32 {
        match self.ty {
            EnemyType::Normal => (10.0 * self.wave_multiplier) as u32,
            EnemyType::Fast => (10.0 * self.wave_multiplier) as u32,
            EnemyType::Tank => (20.0 * self.wave_multiplier) as u32,
        }
    }

    pub fn score_reward(&self) -> u32 {
        match self.ty {
            EnemyType::Normal => (20.0 * self.wave_multiplier) as u32,
            EnemyType::Fast => (35.0 * self.wave_multiplier) as u32,
            EnemyType::Tank => (50.0 * self.wave_multiplier) as u32,
        }
    }
}

impl Component for Enemy {
    type Storage = DenseVecStorage<Self>;
}

impl Collidable for Enemy {
    fn collision_size(&self) -> (f32, f32) {
        match self.ty {
            EnemyType::Tank => (27.0, 20.0),
            _ => (27.0, 12.0)
        }
    }

    fn collider_type(&self) -> ColliderType {
        ColliderType::Enemy
    }
}
