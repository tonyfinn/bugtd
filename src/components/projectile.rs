use crate::components::{Collidable, ColliderType};
use amethyst::ecs::prelude::*;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ProjectileType {
    Laser,
    EnergyOrb,
}

impl ProjectileType {
    pub fn speed(&self) -> f32 {
        match self {
            ProjectileType::Laser => 300.0,
            ProjectileType::EnergyOrb => 75.0,
        }
    }

    pub fn duration(&self) -> f32 {
        match self {
            ProjectileType::Laser => 3.0,
            ProjectileType::EnergyOrb => 15.0,
        }
    }
}

pub struct Projectile {
    pub frame: usize,
    pub elapsed_time: f32,
    pub level: u16,
    pub ty: ProjectileType,
}

impl Projectile {
    pub fn new(ty: ProjectileType, level: u16) -> Projectile {
        Projectile {
            ty,
            elapsed_time: 0.0,
            level,
            frame: 0,
        }
    }

    pub fn damage(&self) -> u16 {
        let base_damage = match self.ty {
            ProjectileType::Laser => 5,
            ProjectileType::EnergyOrb => 5,
        };

        base_damage * self.level
    }
}

impl Collidable for Projectile {
    fn collision_size(&self) -> (f32, f32) {
        match self.ty {
            ProjectileType::Laser => (32.0, 8.0),
            ProjectileType::EnergyOrb => (32.0, 32.0),
        }
    }

    fn collider_type(&self) -> ColliderType {
        ColliderType::Projectile
    }
}

impl Component for Projectile {
    type Storage = DenseVecStorage<Self>;
}
