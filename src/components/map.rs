use amethyst::ecs::prelude::*;
pub struct Map {
    width: u16,
    height: u16,
}

impl Map {
    pub fn new(width: u16, height: u16) -> Map {
        Map { width, height }
    }
}

impl Component for Map {
    type Storage = DenseVecStorage<Self>;
}
