use amethyst::ecs::prelude::*;
use crate::components::{Collidable, ColliderType};
use std::time::Duration;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum EffectType {
    Damage,
    Slow,
}

impl EffectType {
    pub fn duration(&self) -> f32 {
        match self {
            EffectType::Damage => 5.0,
            EffectType::Slow => 5.0,
        }
    }
}

pub struct Effect {
    pub ty: EffectType,
    pub strength: u16,
    pub frame: usize,
    pub spawn_time: Duration,
}

impl Effect {
    pub fn new(ty: EffectType, strength: u16, spawn_time: Duration) -> Effect {
        Effect {
            ty,
            strength,
            spawn_time,
            frame: 0,
        }
    }
}

impl Component for Effect {
    type Storage = DenseVecStorage<Self>;
}

impl Collidable for Effect {
    fn collision_size(&self) -> (f32, f32) {
        match self.ty {
            EffectType::Damage => (64.0, 64.0),
            EffectType::Slow => (96.0, 96.0),
        }
    }

    fn collider_type(&self) -> ColliderType {
        ColliderType::Effect
    }
}