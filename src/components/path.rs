use amethyst::ecs::*;

pub use crate::resources::pathfinder::Path;

impl Component for Path {
    type Storage = DenseVecStorage<Self>;
}
