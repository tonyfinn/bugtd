use crate::components::EnemyType;

use amethyst::core::ecs::prelude::*;

use std::collections::VecDeque;
use std::time::Duration;

pub struct Wave {
    pub id: u16,
    pub enemy_list: VecDeque<EnemyType>,
    pub multiplier: f32,
    pub started: bool,
    pub cleared: bool,
    pub start_time: Duration,
    pub spawn_cooldown: Duration,
}

impl Wave {
    pub fn new(
        id: u16, 
        multiplier: f32, 
        start_time: Duration, 
        spawn_cooldown: Duration,
        enemy_list: &[EnemyType]
    ) -> Wave {
        Wave {
            id,
            multiplier,
            start_time,
            spawn_cooldown,
            started: false,
            cleared: false,
            enemy_list: enemy_list.iter().cloned().collect(),
        }
    }
}

impl Component for Wave {
    type Storage = DenseVecStorage<Self>;
}
