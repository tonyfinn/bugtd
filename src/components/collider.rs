use amethyst::ecs::prelude::*;
pub use ncollide2d::pipeline::object::CollisionObjectSlabHandle as CollisionHandle;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
pub enum ColliderType {
    Core = 1,
    Enemy = 2,
    Projectile = 3,
    Effect = 4,
}

impl ColliderType {
    pub fn group_id(&self) -> usize {
        *self as usize
    }

    pub fn collides_with(&self) -> Vec<usize> {
        match self {
            ColliderType::Core => vec![ColliderType::Enemy.group_id()],
            ColliderType::Enemy => vec![
                ColliderType::Core.group_id(),
                ColliderType::Projectile.group_id(),
                ColliderType::Effect.group_id(),
            ],
            ColliderType::Projectile => vec![ColliderType::Enemy.group_id()],
            ColliderType::Effect => vec![ColliderType::Enemy.group_id()],
        }
    }
}

pub trait Collidable {
    fn collision_size(&self) -> (f32, f32);
    fn collider_type(&self) -> ColliderType;
}

pub struct Collider {
    pub handle: CollisionHandle,
}

impl From<CollisionHandle> for Collider {
    fn from(handle: CollisionHandle) -> Collider {
        Collider { handle }
    }
}

impl Component for Collider {
    type Storage = DenseVecStorage<Self>;
}
