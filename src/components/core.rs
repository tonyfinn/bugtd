use crate::components::{Collidable, ColliderType};
use crate::utils::{GridObject, GridPos, GridSize};
use amethyst::ecs::*;

pub struct Core {
    pub hp: usize,
    pub grid_x: u16,
    pub grid_y: u16,
}

impl Core {
    pub fn new(grid_x: u16, grid_y: u16) -> Core {
        Core {
            grid_x,
            grid_y,
            hp: 9,
        }
    }
}

impl Component for Core {
    type Storage = DenseVecStorage<Self>;
}

impl Collidable for Core {
    fn collision_size(&self) -> (f32, f32) {
        (96.0, 96.0)
    }

    fn collider_type(&self) -> ColliderType {
        ColliderType::Core
    }
}

impl GridObject for Core {
    fn grid_pos(&self) -> GridPos {
        GridPos::new(self.grid_x, self.grid_y)
    }

    fn grid_size(&self) -> GridSize {
        GridSize::new(3, 3)
    }
}
