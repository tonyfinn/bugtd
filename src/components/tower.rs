use crate::components::ProjectileType;
use crate::utils::{GridObject, GridPos, GridSize};
use amethyst::core::ecs::prelude::*;
use std::time::Duration;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TowerType {
    Laser,
    Energy,
    Magnet,
}

impl TowerType {
    pub fn projectile_type(&self) -> Option<ProjectileType> {
        match self {
            TowerType::Laser => Some(ProjectileType::Laser),
            TowerType::Energy => Some(ProjectileType::EnergyOrb),
            _ => None,
        }
    }

    pub fn cooldown(&self) -> Duration {
        Duration::from_secs(match self {
            TowerType::Laser => 2,
            _ => 10,
        })
    }

    pub fn cost(&self) -> u32 {
        match self {
            TowerType::Laser => 25,
            TowerType::Energy => 200,
            TowerType::Magnet => 150,
        }
    }
}

pub struct Tower {
    pub ty: TowerType,
    pub grid_x: u16,
    pub grid_y: u16,
    pub level: usize,
    pub last_attack_time: Duration,
}

impl Tower {
    pub fn new(ty: TowerType, grid_x: u16, grid_y: u16) -> Tower {
        Tower {
            ty,
            grid_x,
            grid_y,
            level: 1,
            ..Default::default()
        }
    }

    pub fn range(&self) -> f32 {
        match self.ty {
            TowerType::Laser => 150.0,
            TowerType::Energy => 280.0,
            TowerType::Magnet => 96.0,
        }
    }
}

impl GridObject for Tower {
    fn grid_pos(&self) -> GridPos {
        GridPos::new(self.grid_x, self.grid_y)
    }

    fn grid_size(&self) -> GridSize {
        GridSize::new(2, 2)
    }

    fn pathfinding_cost(&self) -> u32 {
        10000
    }
}

impl Default for Tower {
    fn default() -> Tower {
        Tower {
            ty: TowerType::Laser,
            grid_x: 5,
            grid_y: 5,
            level: 1,
            last_attack_time: Duration::from_secs(0),
        }
    }
}

impl Component for Tower {
    type Storage = DenseVecStorage<Self>;
}
