use amethyst::core::math::{Point2, Rotation2, Vector2};
use amethyst::core::Transform;

pub const BASIC_TILE_SIZE: f32 = 32.0;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct GridPos {
    pub x: u16,
    pub y: u16,
}

impl GridPos {
    pub fn new(x: u16, y: u16) -> GridPos {
        GridPos { x, y }
    }

    pub fn from_world_pos(x: f32, y: f32) -> GridPos {
        GridPos {
            x: (x / BASIC_TILE_SIZE).floor() as u16,
            y: (y / BASIC_TILE_SIZE).floor() as u16,
        }
    }

    pub fn from_transform(transform: &Transform) -> GridPos {
        GridPos::from_world_pos(transform.translation().x, transform.translation().y)
    }

    pub fn as_world_px(&self) -> (f32, f32) {
        (
            f32::from(self.x) * BASIC_TILE_SIZE,
            f32::from(self.y) * BASIC_TILE_SIZE,
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct GridSize {
    pub w: u16,
    pub h: u16,
}

impl GridSize {
    pub fn new(w: u16, h: u16) -> GridSize {
        GridSize { w, h }
    }

    fn as_world_px(&self) -> (f32, f32) {
        (
            f32::from(self.w) * BASIC_TILE_SIZE,
            f32::from(self.h) * BASIC_TILE_SIZE,
        )
    }
}

pub trait GridObject {
    fn grid_pos(&self) -> GridPos;
    fn grid_size(&self) -> GridSize;

    fn pathfinding_cost(&self) -> u32 {
        0
    }

    fn transform(&self) -> Transform {
        let (px_x, px_y) = self.grid_pos().as_world_px();
        let (px_w, px_h) = self.grid_size().as_world_px();
        let mut transform = Transform::default();
        transform.set_translation_xyz(px_x + (px_w / 2.0), px_y + (px_h / 2.0), 0.1);
        transform
    }

    fn contains_point(&self, grid_x: u16, grid_y: u16) -> bool {
        let pos = self.grid_pos();
        let size = self.grid_size();

        (pos.x <= grid_x && pos.x + size.w > grid_x)
            && (pos.y <= grid_y && pos.y + size.h > grid_y)
    }
}

// Returns the vector from first to second
pub fn vector2d_between(source: &Transform, target: &Transform) -> Vector2<f32> {
    let source_point = Point2::from(source.translation().xy());
    let target_point = Point2::from(target.translation().xy());
    target_point - source_point
}

// Returns the angle of a given vector relative ot the x-axis
pub fn angle_2d(vec: &Vector2<f32>) -> f32 {
    Rotation2::rotation_between(&Vector2::x_axis(), &vec).angle()
}
