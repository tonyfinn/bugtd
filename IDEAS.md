# Tower Defense

## Technologies

* Amethyst game engine
* Rust programming language

## TODO List

[X] Towers
[X] Enemies
[X] Waves
[X] Tower Placement
   - Grid
   - Ghosts
[X] Tower Effects
[X] Core
[X] Pathfinding
[X] Tower variety
[X] Animations
[ ] Spawn Zones
[X] Upgrades
[X] Enemy variety
[ ] Sound
[ ] Better UI